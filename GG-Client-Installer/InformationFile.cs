﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GG_Client_Installer
{
    class InformationFile
    {
        public string FileName { get { return fileName; } }
        public long FileSizeServer { get { return fileSizeServer; } }
        public DateTime FileDateServer { get { return fileDateServer; } }
        public Version FileVersionServer { get { return fileVersionServer; }  set { fileVersionServer= value; }  }
        public Version FileVersionLocal { get { return fileVersionLocal; } set { fileVersionLocal = value; } }
        public string ProductName { get { return productName; } set { productName = value; } }
        public string TemporaryFileStorage { get { return temporaryFileStorage; } set { temporaryFileStorage = value; } }
        public string Description { get { return description; } set { description = value; } }
        public readonly decimal BYTE_TO_MEGA_BYTE_BINARY = 0.00000095367432M;
         
        private string fileName;
        private string productName;
        private long fileSizeServer;
        private DateTime fileDateServer;
        private Version fileVersionServer;
        private Version fileVersionLocal;
        private string temporaryFileStorage;
        private string description;


        public InformationFile(string metaDataFile)
        {
            string[] parts = metaDataFile.Split(new[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            fileName = parts[3];
            fileSizeServer = Convert.ToInt32(parts[2]);
            fileDateServer = DateTime.ParseExact(string.Format("{0} {1}", parts[0], parts[1]), "MM-dd-yyyy hh:mmtt", CultureInfo.InvariantCulture);
        }
    }
}
