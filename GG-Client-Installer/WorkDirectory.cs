﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Util;



namespace GG_Client_Installer
{
    /// <summary>
    /// Create and download fiel for the program work it
    /// </summary>
    class WorkDirectory
    {
        public ResultBL Result;
        public readonly string DEFAULT_DOWNLOAD_DIRECTORY = "ggClient";
        public readonly string DEFAULT_WIZARD_DIRECTORY = "Wizards";
        public UtilBL util = new UtilBL();

        public WorkDirectory()
        {
            Result = new ResultBL();
        }

        /// <summary>
        /// Download a list of files and create tempory directory to download they
        /// </summary>
        /// <param name="informationFiles">List of files</param>
        /// <param name="ftp">FTP server instance</param>
        /// <param name="pathDirectory">Path to save files</param>
        /// <returns>Adding to de list of file information about it (version, name, product name)</returns>
        public List<InformationFile> CreateTemporaryWorkDirectory(List<InformationFile> informationFiles
            , FTPService ftp
            , string downloadDirectory = "")
        {
            string pathDirectory = CreateDirectory(downloadDirectory);
            informationFiles = DownloadFiles(informationFiles, ftp, pathDirectory);
            return informationFiles;
        }

        /// <summary>
        /// Find the information of a program installed in Windows
        /// </summary>
        /// <param name="informationFiles">List of program to find in the property Program Name</param>
        /// <returns>will add information in the "InfromationFile" if the program is installed on Windows system.</returns>
        public List<InformationFile> GetInformationFromInstalled(List<InformationFile> informationFiles)
        {
            foreach (InformationFile informationFile in informationFiles)
            {
                informationFile.FileVersionLocal = util.String2Version(util.GetVersionFromInstalledProgram(informationFile.ProductName));
            }
            return informationFiles;
        }

        private List<InformationFile> DownloadFiles(List<InformationFile> informationFiles
            , FTPService ftp
            , string pathDirectory)
        {
            try
            {
                foreach (InformationFile informationFile in informationFiles)
                {

                    string foundFile = Path.Combine(pathDirectory, informationFile.FileName);
                    if (File.Exists(foundFile))
                    {

                            File.Delete(foundFile);
                    }
                    ftp.DownloadFile(file: informationFile.FileName
                        , pathToSave: pathDirectory);
                    informationFile.FileVersionServer = util.String2Version(
                    util.GetPropertyString(foundFile, util.FileProperty.PROPERTY_VERSION));
                    informationFile.ProductName = util.GetPropertyString(foundFile, util.FileProperty.PROPERTY_NAME);
                    informationFile.Description = util.GetPropertyString(foundFile, util.FileProperty.PROPERTY_COMMENT);
                    informationFile.TemporaryFileStorage = foundFile;
                }
            }
            catch (IOException ex)
            {
                Result.AddError(string.Format("Unable to delete, download or read file"), ex, "WORKDIRECTORY-001");
            }
            finally
            {

            }
            return informationFiles;
        }

        


        private string CreateDirectory(string downloadDirectory)
        {
            if (downloadDirectory.Equals(""))
            {
                downloadDirectory = DEFAULT_DOWNLOAD_DIRECTORY;
            }
            string pathSearch = Path.Combine(System.IO.Path.GetTempPath(), downloadDirectory);
            if (!Directory.Exists(pathSearch))
            {
                Directory.CreateDirectory(pathSearch);
            }
            return pathSearch;
        }
    }
}
