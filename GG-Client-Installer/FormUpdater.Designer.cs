﻿namespace GG_Client_Installer
{
    partial class FormUpdater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdater));
            this.textBoxUrl = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ButtonDownloadServer = new System.Windows.Forms.Button();
            this.ButtonCheckForServerUpdates = new System.Windows.Forms.Button();
            this.DataGridViewComponent = new System.Windows.Forms.DataGridView();
            this.Install = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileSizeServerMegaByte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileVersionServer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileVersionLocal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewComponent)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxUrl
            // 
            this.textBoxUrl.Location = new System.Drawing.Point(28, 38);
            this.textBoxUrl.Name = "textBoxUrl";
            this.textBoxUrl.ReadOnly = true;
            this.textBoxUrl.Size = new System.Drawing.Size(327, 20);
            this.textBoxUrl.TabIndex = 15;
            this.textBoxUrl.Text = "ggcommunity.southcentralus.cloudapp.azure.com";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(224, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "The URL will be got the installation packages.";
            // 
            // ButtonDownloadServer
            // 
            this.ButtonDownloadServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonDownloadServer.Enabled = false;
            this.ButtonDownloadServer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ButtonDownloadServer.Location = new System.Drawing.Point(493, 263);
            this.ButtonDownloadServer.Name = "ButtonDownloadServer";
            this.ButtonDownloadServer.Size = new System.Drawing.Size(138, 23);
            this.ButtonDownloadServer.TabIndex = 24;
            this.ButtonDownloadServer.Text = "Download / Install";
            this.ButtonDownloadServer.UseVisualStyleBackColor = true;
            this.ButtonDownloadServer.Click += new System.EventHandler(this.ButtonDownloadServer_Click);
            // 
            // ButtonCheckForServerUpdates
            // 
            this.ButtonCheckForServerUpdates.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ButtonCheckForServerUpdates.Location = new System.Drawing.Point(28, 64);
            this.ButtonCheckForServerUpdates.Name = "ButtonCheckForServerUpdates";
            this.ButtonCheckForServerUpdates.Size = new System.Drawing.Size(167, 23);
            this.ButtonCheckForServerUpdates.TabIndex = 25;
            this.ButtonCheckForServerUpdates.Text = "Check For Server Updates";
            this.ButtonCheckForServerUpdates.UseVisualStyleBackColor = true;
            this.ButtonCheckForServerUpdates.Click += new System.EventHandler(this.btnCheckForServerUpdates_Click);
            // 
            // DataGridViewComponent
            // 
            this.DataGridViewComponent.AllowUserToAddRows = false;
            this.DataGridViewComponent.AllowUserToDeleteRows = false;
            this.DataGridViewComponent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGridViewComponent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DataGridViewComponent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewComponent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Install,
            this.FileName,
            this.Description,
            this.FileSizeServerMegaByte,
            this.FileVersionServer,
            this.FileVersionLocal,
            this.Status});
            this.DataGridViewComponent.Location = new System.Drawing.Point(13, 93);
            this.DataGridViewComponent.Name = "DataGridViewComponent";
            this.DataGridViewComponent.Size = new System.Drawing.Size(618, 164);
            this.DataGridViewComponent.TabIndex = 26;
            // 
            // Install
            // 
            this.Install.DataPropertyName = "Install";
            this.Install.Frozen = true;
            this.Install.HeaderText = "Install";
            this.Install.Name = "Install";
            this.Install.Width = 40;
            // 
            // FileName
            // 
            this.FileName.DataPropertyName = "FileName";
            this.FileName.HeaderText = "FileName";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Width = 76;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 85;
            // 
            // FileSizeServerMegaByte
            // 
            this.FileSizeServerMegaByte.DataPropertyName = "FileSizeServerMegaByte";
            this.FileSizeServerMegaByte.HeaderText = "FileSizeServerMegaByte";
            this.FileSizeServerMegaByte.Name = "FileSizeServerMegaByte";
            this.FileSizeServerMegaByte.ReadOnly = true;
            this.FileSizeServerMegaByte.Width = 147;
            // 
            // FileVersionServer
            // 
            this.FileVersionServer.DataPropertyName = "FileVersionServer";
            this.FileVersionServer.HeaderText = "FileVersionServer";
            this.FileVersionServer.Name = "FileVersionServer";
            this.FileVersionServer.ReadOnly = true;
            this.FileVersionServer.Width = 114;
            // 
            // FileVersionLocal
            // 
            this.FileVersionLocal.DataPropertyName = "FileVersionLocal";
            this.FileVersionLocal.HeaderText = "FileVersionLocal";
            this.FileVersionLocal.Name = "FileVersionLocal";
            this.FileVersionLocal.ReadOnly = true;
            this.FileVersionLocal.Width = 109;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Width = 62;
            // 
            // FormUpdater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 302);
            this.Controls.Add(this.DataGridViewComponent);
            this.Controls.Add(this.ButtonCheckForServerUpdates);
            this.Controls.Add(this.ButtonDownloadServer);
            this.Controls.Add(this.textBoxUrl);
            this.Controls.Add(this.label5);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormUpdater";
            this.Text = "GRIN-Global Client Update";
            this.Load += new System.EventHandler(this.FormUpdater_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewComponent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxUrl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ButtonDownloadServer;
        private System.Windows.Forms.Button ButtonCheckForServerUpdates;
        private System.Windows.Forms.DataGridView DataGridViewComponent;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Install;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileSizeServerMegaByte;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileVersionServer;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileVersionLocal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}

