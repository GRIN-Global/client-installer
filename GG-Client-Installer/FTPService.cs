﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace GG_Client_Installer
{
    class FTPService 
    {
        /// <summary>
        /// The class will get the information about the result of the process (fail or success).
        /// </summary>
        public ResultBL Result;

        private string Url;
        private string Username;
        private string Password;
        private bool UsePassive;


        /// <summary>
        /// The first constructor receives the basic URL and other parameters
        /// </summary>
        /// <param name="url">FTP service basic url</param>
        /// <param name="username">FTP service user</param>
        /// <param name="password">FTP service password</param>
        /// <param name="usePassive">FTP service activate use passive mode</param>
        public FTPService(string url, string username, string password, bool usePassive = false)
        {
            Url = url;
            Username = username;
            Password = password;
            UsePassive = usePassive;
            Result = new ResultBL();
        }

        /// <summary>
        /// Download the file and save it in a specific local folder
        /// </summary>
        /// <param name="file">file to download</param>
        /// <param name="pathToSave">path to save file</param>
        /// <param name="urlComplement">folder specific </param>
        /// <returns></returns>
        public string DownloadFile(string file, string pathToSave, string urlComplement = "")
        {
            try
            {
                string url = Url;
                if (urlComplement != "")
                {
                    url += "/" + urlComplement;
                }
                url += "/" + file;
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.UsePassive = UsePassive;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(Username, Password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                using (FileStream writer = new FileStream(Path.Combine(pathToSave, file), FileMode.Create))
                {
                    long length = response.ContentLength;
                    int bufferSize = 2048;
                    int readCount;
                    byte[] buffer = new byte[2048];
                    readCount = responseStream.Read(buffer, 0, bufferSize);
                    while (readCount > 0)
                    {
                        writer.Write(buffer, 0, readCount);
                        readCount = responseStream.Read(buffer, 0, bufferSize);
                    }
                    writer.Close();
                }
                responseStream.Close();
                reader.Close();
                response.Close();
                return Path.Combine(pathToSave, file);
            }
            catch (Exception ex)
            {
                Result.AddError(string.Format("Unable to download file in FTP [{0}]", file)
                    , ex
                    , "FTP-0101");
                return "";
            }
        }

        /// <summary>
        /// List all the files that are in folder or root folder of the ftp service
        /// </summary>
        /// <param name="urlComplement">Specific folder</param>
        /// <returns></returns>
        public List<string> ListFiles(string urlComplement = "")
        {
            try
            {
                string url = Url;
                if (urlComplement != "")
                {
                    url += "/" + urlComplement;
                }
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                request.UsePassive = UsePassive;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();
                reader.Close();
                response.Close();
                return new List<string>(names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
            }
            catch (Exception ex)
            {
                Result.AddError("Unable to list all files in FTP", ex, "FTP-0101");
                return new List<string>();
            }
        }

        /// <summary>
        /// Lis the all information about directory or FTP root
        /// </summary>
        /// <param name="urlComplement">Specify the directory on the FTP</param>
        /// <returns>List of file found</returns>
        public List<InformationFile> ListInformationFiles(string urlComplement = "")
        {
            try
            {
                string url = Url;
                if (urlComplement != "")
                {
                    url += "/" + urlComplement;
                }
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                request.Credentials = new NetworkCredential(Username, Password);
                request.UsePassive = UsePassive;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();
                reader.Close();
                response.Close();
                return SplitFile(names);
            }
            catch (Exception ex)
            {
                Result.AddError("Unable to list file from FTP", ex, "FTP-0201");
                return new List<InformationFile>();
            }
        }

        private List<InformationFile> SplitFile(string names)
        {
            List<InformationFile> informationfiles = new List<InformationFile>(); 
            foreach (string name in names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                InformationFile informationFile = new InformationFile(name);
                informationfiles.Add(informationFile);
            }
            return informationfiles;
        }
    }
}
