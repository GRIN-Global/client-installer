﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Util;

namespace GG_Client_Installer
{
    public partial class FormUpdater : Form
    {
        private string url = string.Empty;
        private string username = string.Empty;
        private string password = string.Empty;
        private string SETTINGS_FILE = "./settings.xml";

        private List<InformationFile> informationFiles;

        public FormUpdater()
        {
            InitializeComponent();
            try
            {
                GetCredentialFtp();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void GetCredentialFtp()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(SETTINGS_FILE);
            XmlNode nodeParent = xmlDoc.DocumentElement.SelectSingleNode("/ftp");
            foreach (XmlNode node in nodeParent.ChildNodes)
            {
                switch (node.Name)
                {
                    case "url":
                        url = node.InnerText;
                        textBoxUrl.Text = url;
                        break;
                    case "username":
                        username = node.InnerText;
                        break;
                    case "password":
                        password = node.InnerText;
                        break;
                }
            }
            if(url == string.Empty || username == string.Empty || password == string.Empty)
            {
                throw new System.InvalidOperationException("The setting file was not found or the setting file doesn't have the necessary parameters.");
            }
        }

        private void FormUpdater_Load(object sender, EventArgs e)
        {

        }

        private void btnCheckForServerUpdates_Click(object sender, EventArgs e)
        {
            ResultBL result = new ResultBL();
            ButtonDownloadServer.Enabled = false;
            try { 
                result = UpdateGrid();
                
            }
            catch (Exception ex)
            {
                result.AddError("Exception error ", ex, "VIST-001");
            }
            if (!result.IsContinue())
            {
                MessageBox.Show(result.Message, result.Title);
            }
            ButtonCheckForServerUpdates.Enabled = true;
        }

        private ResultBL UpdateGrid()
        {
            ResultBL result = new ResultBL();
            string lastError = string.Empty;
            ButtonCheckForServerUpdates.Enabled = false;
            DataGridViewComponent.DataSource = null;
            DataGridViewComponent.Refresh();
            FTPService ftp = new FTPService("ftp://"+url, username, password);
            WorkDirectory dir = new WorkDirectory();
            List<InformationFile> inf = ftp.ListInformationFiles();
            if (result.IsContinue(ftp.Result))
            {
                inf = dir.CreateTemporaryWorkDirectory(inf, ftp);
            }
            if (result.IsContinue(dir.Result))
            {
                informationFiles = dir.GetInformationFromInstalled(inf);
            }
            if (result.IsContinue(dir.Result))
            {
                DataGridViewComponent.DataSource = GetDataGrid(informationFiles);
                DataGridViewComponent.Refresh();
            }
            return result;
        }

        private string AddError(string newError, string lastError)
        {
            if(lastError == string.Empty)
            {
                if(newError != null && newError != string.Empty)
                {
                    lastError = newError;
                }
            }
            return lastError;
        }

        private List<InformationFileGrid> GetDataGrid(List<InformationFile> sources)
        {
            List<InformationFileGrid> destinys = new List<InformationFileGrid>();
            foreach(InformationFile source in sources)
            {
                InformationFileGrid destiny = new InformationFileGrid() {
                    FileName = source.FileName,
                    FileSizeServerMegaByte = Math.Round(
                        (Convert.ToDecimal(source.FileSizeServer) * source.BYTE_TO_MEGA_BYTE_BINARY
                        *10))/10,
                    FileVersionLocal = source.FileVersionLocal == null ? "" : source.FileVersionLocal.ToString(),
                    FileVersionServer = source.FileVersionServer == null ? "" : source.FileVersionServer.ToString(),
                    Status = source.FileVersionLocal == null ? "Not installed"
                       : source.FileVersionServer > source.FileVersionLocal
                       ? "Need Update" : "Updated",
                    Description = source.Description,
                    Install = source.FileVersionLocal == null ? true : false
                };
                if(destiny.Install == true)
                {
                    ButtonDownloadServer.Enabled = true;
                }
                destinys.Add(destiny);
            }
            return destinys;
        }

        private void ButtonDownloadServer_Click(object sender, EventArgs e)
        {
            List<DataGridViewCell> installs = DataGridViewComponent
                .Rows
                .Cast<DataGridViewRow>()
                .Where(row => (bool)row.Cells["Install"].Value == true)
                .Select(row => row.Cells["FileName"]).ToList();
            if (installs.Count > 0)
            {
                ButtonDownloadServer.Enabled = false;
                foreach(DataGridViewCell install in installs)
                {
                    InformationFile informationFile = informationFiles.Where(row => row.FileName == (string)install.Value).DefaultIfEmpty(null).FirstOrDefault();
                    if(informationFile != null)
                    {
                        Process.Start(informationFile.TemporaryFileStorage);
                    }
                }
            }
        }
    }
}
