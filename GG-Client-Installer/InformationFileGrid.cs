﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GG_Client_Installer
{
    class InformationFileGrid
    {
        public bool Install { set; get; }
        public string FileName { set; get; }
        public string Description { set; get; }
        public decimal FileSizeServerMegaByte { set; get; }
        public string FileVersionServer { set; get; }
        public string FileVersionLocal { set; get; }
        public string Status { set; get; }
        
    }
}
