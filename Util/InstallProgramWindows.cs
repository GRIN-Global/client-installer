﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    class InstallProgramWindows
    {
        /// <summary>
        /// Get the verion of program install in windows
        /// </summary>
        /// <param name="programName">Program to search</param>
        /// <returns>Version the program installed in case not found return empty string</returns>
        public string GetVersionFromInstalledProgram(string programName)
        {
            string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            RegistryKey key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    string displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(programName))
                    {
                        return subkey.GetValue("DisplayVersion") as string;
                    }
                }
                key.Close();
            }
            registryKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
            key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    string displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(programName))
                    {
                        return subkey.GetValue("DisplayVersion") as string;
                    }
                }
                key.Close();
            }
            return string.Empty;
        }
    }
}
