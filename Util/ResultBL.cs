﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public class ResultBL
    {
        /// <summary>
        /// Message of error or warinig
        /// </summary>
        public string Message { get { return message; } }
        /// <summary>
        /// Id unic the error to search information about it
        /// </summary>
        public string Id { get { return id; } }
        /// <summary>
        /// Title to set in message box
        /// </summary>
        public string Title {
            get {
                return isWarning ?
                  string.Format("Warning [{0}]", id)
                  : string.Format("Error [{0}]", id);
            }
        }

        private string message;
        private bool isContinue;
        private bool isWarning;
        private string id;

        public ResultBL()
        {
            message = string.Empty;
            isContinue = true;
            isWarning = false;
        }

        /// <summary>
        /// Will add an error if there doesn't have an error previous
        /// </summary>
        /// <param name="_message">Message from error</param>
        /// <param name="_id">Unique id to identify the error</param>
        /// <param name="_isWarning">Indicate if error is not critical and stop the program</param>
        public void AddError(string _message,string _id ,bool _isWarning = false)
        {
            if (isContinue)
            {
                message = _message;
                id = _id;
                isContinue = false;
                if (_isWarning)
                {
                    isContinue = true;
                    isWarning = _isWarning;
                }
            }
        }

        /// <summary>
        /// Will add an error if there doesn't have an error previous
        /// </summary>
        /// <param name="_message">Message from error</param>
        /// <param name="_id">Unique id to identify the error</param>
        /// <param name="_isWarning">Indicate if error is not critical and stop the program</param>
        /// <param name="_ex">Exception to add de message to the property message</param>
        public void AddError(string _message, Exception _ex,string _id, bool _isWarning = false)
        {
            if (isContinue)
            {
                message = string.Format("{1} [{0}]", _message, _ex.Message);
                id = _id;
                isContinue = false;
                if (_isWarning)
                {
                    isContinue = true;
                    isWarning = _isWarning;
                }
            }
        }
        /// <summary>
        /// will continue if error doesn't exist
        /// </summary>
        /// <param name="result">Adding another result to unify in this result</param>
        /// <returns>will return true if error dosen't exist</returns>
        public bool IsContinue(ResultBL result)
        {
            if(isContinue && !result.IsContinue())
            {
                isContinue = false;
                message = result.message;
                id = result.id;
                isWarning = result.isWarning;
            }
            return isContinue;
        }

        /// <summary>
        /// will continue if error doesn't exist
        /// </summary>
        /// <returns>will return true if error dosen't exist</returns>
        public bool IsContinue() {
            return isContinue;
        }
        /// <summary>
        /// Will continue if the warning doesn't exist
        /// </summary>
        /// <returns>will return true if warning dosen't exist</returns>
        public bool IsWarning()
        {
            return isWarning;
        }
    }
}
