﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Deployment.WindowsInstaller;
using System.IO;
using System.Diagnostics;

namespace Util
{
    class MsiExeProperty
    {
        private MSIPROPERTY property = new MSIPROPERTY();

        /// <summary>
        /// Covert string property version into object Version
        /// </summary>
        /// <param name="versionString">string to convert</param>
        /// <returns>Version transformed on error return null </returns>
        public Version String2Version(string versionString)
        {
            Version version = null;
            string[] parts = versionString.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Count() == 4)
            {
                version = new Version(string.Format("{0}.{1}.{2}.{3}"
                    , parts[0], parts[1], parts[2], parts[3]));
            }
            return version;
        }

        /// <summary>
        /// Get the one property in string
        /// </summary>
        /// <param name="pathToField">directory to file</param>
        /// <param name="property">Constante define in MSIPROPERTY</param>
        /// <returns>string with property if not found return empty string</returns>
        public string GetPropertyString(string pathToFile, string property)
        {
            string propertyValue = string.Empty;
            try
            {
                FileInfo fi = new FileInfo(pathToFile);

                switch (fi.Extension)
                {
                    case ".msi":
                        propertyValue = GetPropertyMsiString(pathToFile, property);
                        break;
                    case ".exe":
                        propertyValue = GetPropertyExeString(pathToFile, property);
                        break;
                }
            }catch(Exception ex)
            {
                throw new System.FieldAccessException(string.Format("The file can't load property file [{0}] {1}", pathToFile, ex.Message));
            }
            return propertyValue;
        }

        private string GetPropertyExeString(string pathToFile, string property)
        {
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(pathToFile);
            
            string propertyFound = string.Empty;
            switch (property)
            {
                case "VERSION":
                    propertyFound = string.Format("{0}.{1}.{2}.{3}", fileVersionInfo.FileMajorPart, fileVersionInfo.FileMinorPart, fileVersionInfo.FileBuildPart, fileVersionInfo.FilePrivatePart);
                    break;
                case "NAME":
                    propertyFound = string.Format("{0}", fileVersionInfo.ProductName);
                    break;
                case "COMMENT":
                    propertyFound = string.Format("{0}", fileVersionInfo.FileDescription);
                    break;
            }
            return propertyFound;
        }

        private string GetPropertyMsiString(string pathToFile, string property)
        {
            string propertyValue = string.Empty;
            switch (property)
            {
                case "NAME":
                    propertyValue = GetMsiProperty(pathToFile, "ProductName");
                    break;
                case "VERSION":
                    propertyValue = GetMsiProperty(pathToFile, "ProductVersion");
                    break;
                case "COMMENT":
                    using (Database database = new Database(pathToFile, DatabaseOpenMode.ReadOnly))
                    {
                        propertyValue = database.SummaryInfo.Comments;
                        database.Close();
                    }
                    break;
            }

            return propertyValue;
        }

        private string GetMsiProperty(string pathToFile, string property)
        {
            string propertyValue = string.Empty;
            using (var database = new Database(pathToFile, DatabaseOpenMode.ReadOnly))
            {
                using (var view = database.OpenView(database.Tables["Property"].SqlSelectString))
                {
                    view.Execute();
                    foreach (var rec in view) using (rec)
                    {
                        if (rec.GetString("Property").Equals(property))
                        {
                            propertyValue = rec.GetString("Value");
                        }
                    }
                }
            }
            return propertyValue;
        }
    }
    /// <summary>
    /// Constant to work with property.
    /// </summary>
    public class MSIPROPERTY
    {
        /// <summary>
        /// search a comment in msi or exe
        /// </summary>
        public readonly string PROPERTY_COMMENT = "COMMENT";
        /// <summary>
        /// search the version in msi or exe
        /// </summary>
        public readonly string PROPERTY_VERSION = "VERSION";
        /// <summary>
        /// Search the product name in msi or exe
        /// </summary>
        public readonly string PROPERTY_NAME = "NAME";

    }
}
