﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public class UtilBL
    {
        /// <summary>
        /// Constants to perform actions on downloaded files 
        /// </summary>
        public MSIPROPERTY FileProperty = new MSIPROPERTY();

        /// <summary>
        /// Get the one property in string
        /// </summary>
        /// <param name="pathToField">directory to file</param>
        /// <param name="property">Constante define in MSIPROPERTY</param>
        /// <returns>string with property if not found return empty string</returns>
        public string GetPropertyString(string pathToField, string property)
        {
            string propertyValue = string.Empty;
            MsiExeProperty msi = new MsiExeProperty();
            return msi.GetPropertyString(pathToField, property);
        }
        /// <summary>
        /// Covert string property version in to object Version
        /// </summary>
        /// <param name="versionString">string to convert</param>
        /// <returns>Version transformed on error return null </returns>
        public Version String2Version(string versionString)
        {
            MsiExeProperty msi = new MsiExeProperty();
            return msi.String2Version(versionString);
        }

        /// <summary>
        /// Get the verion of program install in windows
        /// </summary>
        /// <param name="programName">Program to search</param>
        /// <returns>Version the program installed in case not found return empty string</returns>
        public string GetVersionFromInstalledProgram(string programName)
        {
            InstallProgramWindows installProgramWindows = new InstallProgramWindows();
            return installProgramWindows.GetVersionFromInstalledProgram(programName);
        }
    }
}
